class Software < ActiveRecord::Base
	has_many :communication_channels
	has_many :documentation
	has_many :resource
	has_many :soft_skill
	has_many :technical_skill
end
