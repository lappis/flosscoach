require 'test_helper'

class TechnicalSkillsControllerTest < ActionController::TestCase
  setup do
    @technical_skill = technical_skills(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:technical_skills)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create technical_skill" do
    assert_difference('TechnicalSkill.count') do
      post :create, technical_skill: { description: @technical_skill.description, name: @technical_skill.name }
    end

    assert_redirected_to technical_skill_path(assigns(:technical_skill))
  end

  test "should show technical_skill" do
    get :show, id: @technical_skill
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @technical_skill
    assert_response :success
  end

  test "should update technical_skill" do
    patch :update, id: @technical_skill, technical_skill: { description: @technical_skill.description, name: @technical_skill.name }
    assert_redirected_to technical_skill_path(assigns(:technical_skill))
  end

  test "should destroy technical_skill" do
    assert_difference('TechnicalSkill.count', -1) do
      delete :destroy, id: @technical_skill
    end

    assert_redirected_to technical_skills_path
  end
end
