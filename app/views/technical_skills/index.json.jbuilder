json.array!(@technical_skills) do |technical_skill|
  json.extract! technical_skill, :id, :name, :description
  json.url technical_skill_url(technical_skill, format: :json)
end
