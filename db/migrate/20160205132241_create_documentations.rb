class CreateDocumentations < ActiveRecord::Migration
  def change
    create_table :documentations do |t|
      t.string :name
      t.text :description
      t.text :file

      t.timestamps null: false
    end
  end
end
