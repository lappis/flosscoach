json.array!(@documentations) do |documentation|
  json.extract! documentation, :id, :name, :description, :file
  json.url documentation_url(documentation, format: :json)
end
