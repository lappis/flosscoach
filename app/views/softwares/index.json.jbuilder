json.array!(@softwares) do |software|
  json.extract! software, :id, :name, :description, :page, :contribution_model, :coding_standard, :contribution_standard, :setup_workspace, :issue_tracker, :repository
  json.url software_url(software, format: :json)
end
