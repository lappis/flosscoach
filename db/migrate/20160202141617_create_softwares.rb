class CreateSoftwares < ActiveRecord::Migration
  def change
    create_table :softwares do |t|
      t.string :name
      t.text :description
      t.text :page
      t.text :contribution_model
      t.text :coding_standard
      t.text :contribution_standard
      t.text :setup_workspace
      t.string :issue_tracker
      t.string :repository
      t.references :resources
      t.references :technical_skills
      t.references :soft_skills
      t.references :ducumentation
      t.timestamps null: false
    end
  end
end
