json.array!(@communication_channels) do |communication_channel|
  json.extract! communication_channel, :id, :name, :description, :standards
  json.url communication_channel_url(communication_channel, format: :json)
end
