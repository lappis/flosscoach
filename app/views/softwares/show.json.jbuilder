json.extract! @software, :id, :name, :description, :page, :contribution_model, :coding_standard, :contribution_standard, :setup_workspace, :issue_tracker, :repository, :created_at, :updated_at
