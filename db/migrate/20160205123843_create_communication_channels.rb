class CreateCommunicationChannels < ActiveRecord::Migration
  def change
    create_table :communication_channels do |t|
      t.string :name
      t.text :description
      t.text :standards

      t.timestamps null: false
    end
  end
end
