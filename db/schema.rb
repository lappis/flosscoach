# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160304172113) do

  create_table "communication_channels", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.text     "standards"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "software_id"
  end

  add_index "communication_channels", ["software_id"], name: "index_communication_channels_on_software_id"

  create_table "documentations", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.text     "file"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "resources", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "soft_skills", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "softwares", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.text     "page"
    t.text     "contribution_model"
    t.text     "coding_standard"
    t.text     "contribution_standard"
    t.text     "setup_workspace"
    t.string   "issue_tracker"
    t.string   "repository"
    t.integer  "resources_id"
    t.integer  "technical_skills_id"
    t.integer  "soft_skills_id"
    t.integer  "ducumentation_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "technical_skills", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "username"
    t.string   "password"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
