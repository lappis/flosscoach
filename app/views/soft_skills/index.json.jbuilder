json.array!(@soft_skills) do |soft_skill|
  json.extract! soft_skill, :id, :name, :description
  json.url soft_skill_url(soft_skill, format: :json)
end
