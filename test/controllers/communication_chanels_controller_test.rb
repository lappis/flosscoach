require 'test_helper'

class CommunicationChanelsControllerTest < ActionController::TestCase
  setup do
    @communication_chanel = communication_chanels(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:communication_chanels)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create communication_chanel" do
    assert_difference('CommunicationChanel.count') do
      post :create, communication_chanel: { description: @communication_chanel.description, name: @communication_chanel.name, standards: @communication_chanel.standards }
    end

    assert_redirected_to communication_chanel_path(assigns(:communication_chanel))
  end

  test "should show communication_chanel" do
    get :show, id: @communication_chanel
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @communication_chanel
    assert_response :success
  end

  test "should update communication_chanel" do
    patch :update, id: @communication_chanel, communication_chanel: { description: @communication_chanel.description, name: @communication_chanel.name, standards: @communication_chanel.standards }
    assert_redirected_to communication_chanel_path(assigns(:communication_chanel))
  end

  test "should destroy communication_chanel" do
    assert_difference('CommunicationChanel.count', -1) do
      delete :destroy, id: @communication_chanel
    end

    assert_redirected_to communication_chanels_path
  end
end
