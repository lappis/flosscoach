class AddSoftwareReferenceToCommunicationChannel < ActiveRecord::Migration
  def change
  	add_reference :communication_channels, :software, index: true
  end
end
