class CommunicationChannelsController < ApplicationController
  before_action :set_communication_channel, only: [:show, :edit, :update, :destroy]

  # GET /communication_channels
  # GET /communication_channels.json
  def index
    @communication_channels = CommunicationChannel.all
  end

  # GET /communication_channels/1
  # GET /communication_channels/1.json
  def show
  end

  # GET /communication_channels/new
  def new
    @communication_channel = CommunicationChannel.new
  end

  # GET /communication_channels/1/edit
  def edit
  end

  # POST /communication_channels
  # POST /communication_channels.json
  def create
    @communication_channel = CommunicationChannel.new(communication_channel_params)

    respond_to do |format|
      if @communication_channel.save
        format.html { redirect_to @communication_channel, notice: 'Communication channel was successfully created.' }
        format.json { render :show, status: :created, location: @communication_channel }
      else
        format.html { render :new }
        format.json { render json: @communication_channel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /communication_channels/1
  # PATCH/PUT /communication_channels/1.json
  def update
    respond_to do |format|
      if @communication_channel.update(communication_channel_params)
        format.html { redirect_to @communication_channel, notice: 'Communication channel was successfully updated.' }
        format.json { render :show, status: :ok, location: @communication_channel }
      else
        format.html { render :edit }
        format.json { render json: @communication_channel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /communication_channels/1
  # DELETE /communication_channels/1.json
  def destroy
    @communication_channel.destroy
    respond_to do |format|
      format.html { redirect_to communication_channels_url, notice: 'Communication channel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_communication_channel
      @communication_channel = CommunicationChannel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def communication_channel_params
      params.require(:communication_channel).permit(:name, :description, :standards)
    end
end
