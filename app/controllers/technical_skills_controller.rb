class TechnicalSkillsController < ApplicationController
  before_action :set_technical_skill, only: [:show, :edit, :update, :destroy]

  # GET /technical_skills
  # GET /technical_skills.json
  def index
    @technical_skills = TechnicalSkill.all
  end

  # GET /technical_skills/1
  # GET /technical_skills/1.json
  def show
  end

  # GET /technical_skills/new
  def new
    @technical_skill = TechnicalSkill.new
  end

  # GET /technical_skills/1/edit
  def edit
  end

  # POST /technical_skills
  # POST /technical_skills.json
  def create
    @technical_skill = TechnicalSkill.new(technical_skill_params)

    respond_to do |format|
      if @technical_skill.save
        format.html { redirect_to @technical_skill, notice: 'Technical skill was successfully created.' }
        format.json { render :show, status: :created, location: @technical_skill }
      else
        format.html { render :new }
        format.json { render json: @technical_skill.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /technical_skills/1
  # PATCH/PUT /technical_skills/1.json
  def update
    respond_to do |format|
      if @technical_skill.update(technical_skill_params)
        format.html { redirect_to @technical_skill, notice: 'Technical skill was successfully updated.' }
        format.json { render :show, status: :ok, location: @technical_skill }
      else
        format.html { render :edit }
        format.json { render json: @technical_skill.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /technical_skills/1
  # DELETE /technical_skills/1.json
  def destroy
    @technical_skill.destroy
    respond_to do |format|
      format.html { redirect_to technical_skills_url, notice: 'Technical skill was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_technical_skill
      @technical_skill = TechnicalSkill.find(params[:id])
      puts "="*80, @technical_skill, "="*80
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def technical_skill_params
      params.require(:technical_skill).permit(:name, :description)
    end
end
