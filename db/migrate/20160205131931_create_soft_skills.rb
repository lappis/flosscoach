class CreateSoftSkills < ActiveRecord::Migration
  def change
    create_table :soft_skills do |t|
      t.string :name
      t.text :description

      t.timestamps null: false
    end
  end
end
