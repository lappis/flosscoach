require 'test_helper'

class SoftSkillsControllerTest < ActionController::TestCase
  setup do
    @soft_skill = soft_skills(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:soft_skills)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create soft_skill" do
    assert_difference('SoftSkill.count') do
      post :create, soft_skill: { description: @soft_skill.description, name: @soft_skill.name }
    end

    assert_redirected_to soft_skill_path(assigns(:soft_skill))
  end

  test "should show soft_skill" do
    get :show, id: @soft_skill
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @soft_skill
    assert_response :success
  end

  test "should update soft_skill" do
    patch :update, id: @soft_skill, soft_skill: { description: @soft_skill.description, name: @soft_skill.name }
    assert_redirected_to soft_skill_path(assigns(:soft_skill))
  end

  test "should destroy soft_skill" do
    assert_difference('SoftSkill.count', -1) do
      delete :destroy, id: @soft_skill
    end

    assert_redirected_to soft_skills_path
  end
end
